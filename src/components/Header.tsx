import { Box, Heading } from "@chakra-ui/layout";
import { FC } from "react";

const Header: FC = () => {
  return (
    <Box>
      <Heading textAlign="center">Github issues</Heading>
    </Box>
  );
};
export default Header;
