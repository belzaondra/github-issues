import { Avatar } from "@chakra-ui/avatar";
import { Badge, Box, Flex, Text } from "@chakra-ui/layout";
import React, { FC } from "react";
import GitHubResponse from "../types/githubResponse";

const GitHubIssue: FC<GitHubResponse> = (response) => {
  const openLink = (link: string, e: React.MouseEvent) => {
    e.stopPropagation();
    window.open(link, "_blank");
  };
  return (
    <>
      <Box
        rounded="md"
        borderWidth="1px"
        borderColor="gray.400"
        p={2}
        backgroundColor="gray.100"
        minH="full"
        display="flex"
        flexDir="column"
        _hover={{ backgroundColor: "gray.200" }}
        onClick={(e) => openLink(response.html_url, e)}
        cursor="pointer"
      >
        <Flex align="center" wrap="wrap">
          <Avatar
            onClick={(e) => openLink(response.user.html_url, e)}
            name={response.user.login}
            position="relative"
            cursor="pointer"
            src={response.user.avatar_url}
            mr={2}
          />

          <Text fontWeight="bold" mr={2}>
            #{response.number}
          </Text>
          <Text>{response.title}</Text>
        </Flex>
        <Box flexGrow={1} display="flex" alignContent="end">
          <Flex wrap="wrap" mt="auto">
            {response.labels.map((l, i) => (
              <Badge backgroundColor={"#" + l.color} mr={2} my={2} key={i}>
                {l.name}
              </Badge>
            ))}
          </Flex>
        </Box>
      </Box>
    </>
  );
};
export default GitHubIssue;
