export default interface GitHubResponse {
  id: number;
  number: number;
  title: string;
  html_url: string;
  labels: {
    color: string;
    name: string;
  }[];
  user: {
    avatar_url: string;
    login: string;
    html_url: string;
  };
}
