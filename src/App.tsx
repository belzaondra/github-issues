import { Search2Icon } from "@chakra-ui/icons";
import { Container, Flex } from "@chakra-ui/layout";
import { Box, Input, InputGroup, InputRightAddon } from "@chakra-ui/react";
import { Skeleton } from "@chakra-ui/skeleton";
import axios from "axios";
import React, { FC, useEffect, useState } from "react";
import "./App.css";
import GitHubIssue from "./components/GitHubIssue";
import Header from "./components/Header";
import GitHubResponse from "./types/githubResponse";

const App: FC = () => {
  const [loading, setLoading] = useState(true);
  const [issues, setIssues] = useState<GitHubResponse[]>([]);
  const [possibleIssues, setPossibleIssues] = useState<GitHubResponse[]>([]);
  const [query, setQuery] = useState("");
  const queryChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
    setPossibleIssues(
      issues.filter((i) =>
        i.title.toLocaleLowerCase().includes(query.toLocaleLowerCase())
      )
    );
  };
  const getIssues = async () => {
    try {
      const response = await axios.get<GitHubResponse[]>(
        "https://api.github.com/repos/facebook/react/issues"
      );
      setIssues(response.data);
      setPossibleIssues(response.data);
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    getIssues();
  }, []);
  return (
    <div className="App">
      <Header />
      <Container maxW="container.xl">
        <Flex justifyContent="center">
          <InputGroup maxW="sm">
            <Input
              type="text"
              placeholder="search..."
              value={query}
              onChange={queryChanged}
            />
            <InputRightAddon children={<Search2Icon />} />
          </InputGroup>
        </Flex>
        {loading ? (
          <>
            {[...Array(10)].map((_, i) => (
              <Skeleton my={2} key={i}>
                <div>contents wrapped</div>
                <div>won't be visible</div>
              </Skeleton>
            ))}
          </>
        ) : (
          <>
            <Flex wrap="wrap">
              {possibleIssues.map((i) => (
                <Box key={i.id} w={["100%", "100%", "50%", "25%"]} p={2}>
                  <GitHubIssue {...i} />
                </Box>
              ))}
            </Flex>
          </>
        )}
      </Container>
    </div>
  );
};

export default App;
